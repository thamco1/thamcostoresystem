﻿using System;

namespace ThAmCo.Common.Logs
{
	public class ConsoleLogger : ILogger
	{
		public void Error(string msg, Exception exception = null, params object[] parameters)
		{
			Console.WriteLine(msg);
		}

		public void Info(string msg, params object[] parameters)
		{
			Console.WriteLine(msg);
		}
	}
}