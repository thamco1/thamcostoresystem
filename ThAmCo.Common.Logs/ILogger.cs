﻿using System;

namespace ThAmCo.Common.Logs
{
	public interface ILogger
	{
		void Error(string msg, Exception exception = null, params object[] parameters);
		void Info(string msg, params object[] parameters);
	}
}
