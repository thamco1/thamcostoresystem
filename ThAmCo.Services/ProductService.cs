﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ThAmco.External.Feed;
using ThAmco.External.Feed.Demand.Interface;
using ThAmCo.External.Apis.Dtos;
using ThAmCo.Web.Request;


namespace ThAmCo.Services
{
	public class ProductService : IProductService
	{
		private readonly IDemandProvider demandProvider;

		public ProductService(IDemandProvider demandProvider)
		{
			this.demandProvider = demandProvider;
		}

		public async Task<ProviderResponse<BaseProductSearchDto>> GetProductsAvailability(GetProductsRequest request)
		{
			var demand = this.demandProvider.Create<IDemandAvailabilityForProducts>();
			demand.CategoryId = request.CategoryId;
			demand.BrandId = request.BrandId;
			demand.CategoryName = request.CategoryName;
			demand.MaxPrice = request.MaxPrice;
			demand.MinPrice = request.MinPrice;

			var availability = await demand.ExecuteDemandAsync();

			if (request.Api == null)
			{
				var aggregated = new List<ProductAvailabilitiesDto>();
				foreach (var product in availability.GroupBy(avail => avail.Id))
				{
					aggregated.Add(product.OrderBy(h => h.PriceForOne).FirstOrDefault());
				}

				availability = aggregated;
			}

			return new ProviderResponse<BaseProductSearchDto>
			{
				Data = availability,
			};
		}

		public async Task<BaseProductSearchDto> GetProductDetails(int productId, Api api = Api.Uknown)
		{
			var demand = this.demandProvider.Create<IDemandProviderProductDetails>();
			demand.ProductId = productId;

			var productDetails = await demand.ExecuteDemandAsync();

			throw new NotImplementedException();
		}
	}
}
