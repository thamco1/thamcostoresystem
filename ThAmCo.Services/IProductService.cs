﻿using System.Threading.Tasks;
using ThAmCo.External.Apis.Dtos;
using ThAmCo.Web.Request;
using ThAmCo.Web.Response;

namespace ThAmCo.Services
{
	public interface IProductService
	{
		Task<ProviderResponse<BaseProductSearchDto>> GetProductsAvailability(GetProductsRequest request);
		Task<BaseProductSearchDto> GetProductDetails(int productId, Api api = Api.Uknown);
	}
}
