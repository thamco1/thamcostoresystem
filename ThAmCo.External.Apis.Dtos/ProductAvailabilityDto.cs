﻿using System.Collections.Generic;
using System.Linq;

namespace ThAmCo.External.Apis.Dtos
{
	public class ProductAvailabilityDto
	{
		public ProductAvailabilityDto(Api api, List<ProductDto> products)
		{
			this.Products = products;
			this.Api = api.ToString().ToLower();
		}

		public ProductAvailabilityDto()
		{
			this.Products = Enumerable.Empty<ProductDto>();
		}

		public ProductAvailabilityDto(Api api)
		{
			this.Api = api.ToString().ToLower();
		}

		public Api Api { get; set; }
		public IEnumerable<ProductDto> Products { get; set; } 
	}
}
