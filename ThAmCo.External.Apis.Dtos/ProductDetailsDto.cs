﻿using System;

namespace ThAmCo.External.Apis.Dtos
{
	public class ProductDetailsDto
	{
        public Api Api { get; set; }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public string Ean { get; set; }
        public int Id { get; set; }
        public bool InStock { get; set; }
        public string Name { get; set; }
        public double PriceForOne { get; set; }
        public double PriceForTen { get; set; }
        public DateTime? ExpectedRestock { get; set; }
	}
}
