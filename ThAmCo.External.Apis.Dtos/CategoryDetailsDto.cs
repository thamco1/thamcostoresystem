﻿namespace ThAmCo.External.Apis.Dtos
{
	public class CategoryDetailsDto
	{
		public CategoryDetailsDto(CategoryDto response)
		{
			this.Category = response;
		}

		public CategoryDetailsDto()
		{
		}

		public CategoryDto Category { get; set; }
	}
}