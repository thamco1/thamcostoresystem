﻿using System;

namespace ThAmCo.External.Apis.Dtos
{
	public class ProductAvailabilitiesDto : BaseProductSearchDto
	{
		public string Name { get; set; }
		public int BrandId { get; set; }
		public string Description { get; set; }
		public string Ean { get; set; }
		public bool IsInStock { get; set; }
		public double PriceForOne { get; set; }
		public double PriceForTen { get; set; }
		public DateTime? ExpectedRestock { get; set; }
	}
}
