﻿using System.Collections.Generic;
using System.Linq;

namespace ThAmCo.External.Apis.Dtos
{
	public class BrandCollectionDto
	{
		public BrandCollectionDto(IEnumerable<BrandDto> response)
		{
			this.Brands = response;
		}

		public BrandCollectionDto()
		{
			this.Brands = Enumerable.Empty<BrandDto>();
		}
		public IEnumerable<BrandDto> Brands { get; set; }
	}
}