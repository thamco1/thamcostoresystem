﻿using System.Collections.Generic;
using System.Linq;

namespace ThAmCo.External.Apis.Dtos
{
	public class CategoryCollectionDto
	{
		public CategoryCollectionDto(IEnumerable<CategoryDto> response)
		{
			this.Categories = response;
		}

		public CategoryCollectionDto()
		{
			this.Categories = Enumerable.Empty<CategoryDto>();
		}

		public IEnumerable<CategoryDto> Categories { get; set; }
	}
}