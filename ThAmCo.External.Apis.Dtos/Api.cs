﻿namespace ThAmCo.External.Apis.Dtos
{
	public enum Api
	{
		Uknown = 999,
		Undercutters = 0,
		Dodgydealers = 1,
		Khanskwikimart = 2,
		Bazzasbazzar = 3
	}
}
