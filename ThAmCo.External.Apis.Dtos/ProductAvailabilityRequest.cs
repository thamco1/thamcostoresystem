﻿namespace ThAmCo.External.Apis.Dtos
{
	public class ProductAvailabilityRequest
	{
		public int CategoryId { get; set; }
		public string CategoryName { get; set; }
		public int BrandId { get; set; }
		public double? MinPrice { get; set; }
		public double? MaxPrice { get; set; }
	}
}
