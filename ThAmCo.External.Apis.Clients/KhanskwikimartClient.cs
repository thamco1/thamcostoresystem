﻿using System.Threading.Tasks;
using ThAmCo.Common.Logs;
using ThAmCo.External.Apis.Clients.Interface;
using ThAmCo.External.Apis.Config.Interface;
using ThAmCo.External.Apis.Dtos;

namespace ThAmCo.External.Apis.Clients
{
	public class KhanskwikimartClient : BaseExternalApi, IKhanskwikimartClient
	{
		public KhanskwikimartClient(
			IExternalApiClient apiClient,
			IKhanskwikimartConfig config,
			ILogger logger) 
			: base(apiClient, config, logger, Api.Khanskwikimart)
		{
		}

		public async Task<ProductAvailabilityDto> GetProductAvailability(ProductAvailabilityRequest request)
		{
			return await SendProductAvailabilityRequest(request);
		}

		public async Task<ProductDetailsDto> GetProductDetails(int productId)
		{
			return await SendProductDetailsRequest(productId);
		}

		public async Task<CategoryCollectionDto> GetCategories()
		{
			return await SendCategoriesRequest();
		}

		public async Task<CategoryDetailsDto> GetCategoryDetails(int categoryId)
		{
			return await SendCategoryDetailsRequest(categoryId);
		}

		public async Task<OrderDetailsDto> GetOrderDetails(int orderId)
		{
			return await SendOrderDetailsRequest(orderId);
		}

		public Task<OrderConfirmationDto> ConfirmOrder(OrderToConfirmRequest request)
		{
			throw new System.NotImplementedException();
		}

		public async Task<BrandCollectionDto> GetBrands()
		{
			return await SendBrandsRequest();
		}

		public async Task<BrandDetailsDto> GetBrandDetails(int brandId)
		{
			return await SendBrandDetailsRequest(brandId);
		}
	}
}