﻿using System.Threading.Tasks;
using ThAmCo.External.Apis.Dtos;

namespace ThAmCo.External.Apis.Clients.Interface
{
	public interface IBaseExternalClient
	{
		Task<ProductAvailabilityDto> GetProductAvailability(ProductAvailabilityRequest request);
		Task<ProductDetailsDto> GetProductDetails(int productId);
		Task<CategoryCollectionDto> GetCategories();
		Task<CategoryDetailsDto> GetCategoryDetails(int categoryId);
		Task<OrderDetailsDto> GetOrderDetails(int orderId);
		Task<OrderConfirmationDto> ConfirmOrder(OrderToConfirmRequest request);
		Task<BrandCollectionDto> GetBrands();
		Task<BrandDetailsDto> GetBrandDetails(int brandId);
	}
}
