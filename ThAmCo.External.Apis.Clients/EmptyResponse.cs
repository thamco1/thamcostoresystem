﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThAmCo.External.Apis.Clients
{
    public static class EmptyResponse
    {
        public static T OfType<T>()
        {
            return (T)Activator.CreateInstance(typeof(T), new object[] { });
        }
    }
}
