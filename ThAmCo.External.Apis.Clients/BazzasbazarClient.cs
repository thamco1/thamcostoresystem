﻿using System.Linq;
using System.Threading.Tasks;
using Bazzasbazzaar;
using ThAmCo.External.Apis.Clients.Interface;
using ThAmCo.External.Apis.Dtos;

namespace ThAmCo.External.Apis.Clients
{
	public class BazzasbazarClient : IBazzasbazarClient
	{
		private StoreClient storeClient;

		public BazzasbazarClient()
		{
			this.storeClient = new StoreClient();
		}

		public async Task<ProductAvailabilityDto> GetProductAvailability(ProductAvailabilityRequest request)
		{
			var availability =
				await
					this.storeClient.GetFilteredProductsAsync(request.CategoryId, request.CategoryName, request.MinPrice,
						request.MaxPrice);
			return new ProductAvailabilityDto
			{
				Products = availability.Select(pr => new ProductDto
				{
					Description = pr.Description,
					CategoryId = pr.CategoryId,
					Id = pr.Id,
					Ean = pr.Ean,
					ExpectedRestock = pr.ExpectedRestock,
					Name = pr.Name,
					CategoryName = pr.CategoryName,
					InStock = pr.InStock,
					Price = (decimal)pr.PriceForOne,
				})
			};
		}

		public async Task<ProductDetailsDto> GetProductDetails(int productId)
		{
            var productDetails = await this.storeClient.GetProductByIdAsync(productId);

            return new ProductDetailsDto
            {
                CategoryId = productDetails.CategoryId,
                CategoryName = productDetails.CategoryName,
                Description = productDetails.Description,
                Ean = productDetails.Ean,
                ExpectedRestock = productDetails.ExpectedRestock,
                PriceForTen = productDetails.PriceForTen,
                Id = productDetails.Id,
                InStock = productDetails.InStock,
                Name = productDetails.Name,
                PriceForOne = productDetails.PriceForOne
            };
		}

		public Task<CategoryCollectionDto> GetCategories()
		{
			throw new System.NotImplementedException();
		}

		public Task<CategoryDetailsDto> GetCategoryDetails(int categoryId)
		{
			throw new System.NotImplementedException();
		}

		public Task<OrderDetailsDto> GetOrderDetails(int orderId)
		{
			throw new System.NotImplementedException();
		}

		public Task<OrderConfirmationDto> ConfirmOrder(OrderToConfirmRequest request)
		{
			throw new System.NotImplementedException();
		}

		public Task<BrandCollectionDto> GetBrands()
		{
			throw new System.NotImplementedException();
		}

		public Task<BrandDetailsDto> GetBrandDetails(int brandId)
		{
			throw new System.NotImplementedException();
		}
	}
}
