﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ThAmCo.Common.Logs;
using ThAmCo.External.Apis.Clients.Interface;
using ThAmCo.External.Apis.Config.Interface;
using ThAmCo.External.Apis.Dtos;

namespace ThAmCo.External.Apis.Clients
{
	public abstract class BaseExternalApi
	{
		protected ILogger consoleLogger;
		protected IBaseConfiguration baseConfig;
		protected IExternalApiClient apiClient;
		protected Api api;

		protected BaseExternalApi(IExternalApiClient apiClient, IBaseConfiguration baseConfig, ILogger logger, Api api)
		{
			this.consoleLogger = logger;
			this.baseConfig = baseConfig;
			this.apiClient = apiClient;
			this.api = api;
		}

		protected async Task<ProductAvailabilityDto> SendProductAvailabilityRequest(ProductAvailabilityRequest request)
		{
			try
			{
				var apiRequest = UrlParser.ParseProductsAvailabilityRequest(request);
				consoleLogger.Info($"Availability request -- Api.{api} -- Data: ", apiRequest);

				var result = await apiClient.Get<List<ProductDto>>(apiRequest, baseConfig.ProductsEndpoint);
				consoleLogger.Info($"Availability response -- Api{api} -- Data: ", result);

				var availability = new ProductAvailabilityDto(api, result);
				return availability;
			}
			catch (Exception ex)
			{
				consoleLogger.Error("UndercuttersClient.GetProducts -- Exception", ex);
			}

			return new ProductAvailabilityDto(api);
		}

		protected async Task<ProductDetailsDto> SendProductDetailsRequest(int productId)
		{
			try
			{
				var apiRequest = UrlParser.ParseProductDetailsRequest(productId);
				consoleLogger.Info($"Availability response -- Api{api} -- Data: ", apiRequest);

				var result = await apiClient.Get<ProductDto>(apiRequest, baseConfig.ProductsEndpoint);
				consoleLogger.Info($"Availability response -- Api{api} -- Data: ", result);

				return new ProductDetailsDto(result);
			}
			catch (Exception ex)
			{
				consoleLogger.Error("UndercuttersClient.GetProducts -- Exception", ex);
			}

			return new ProductDetailsDto();
		}

		protected async Task<CategoryCollectionDto> SendCategoriesRequest()
		{
			try
			{
				consoleLogger.Info("Availability request -- Api.Undercutters -- Data: ");

				var result = await apiClient.Get<List<CategoryDto>>(null, baseConfig.CategoriesEndpoitn);
				consoleLogger.Info($"Availability response -- Api{api} -- Data: ", result);

				return new CategoryCollectionDto(result);
			}
			catch (Exception ex)
			{
				consoleLogger.Error("UndercuttersClient.GetProducts -- Exception", ex);
			}

			return new CategoryCollectionDto();
		}

		protected async Task<CategoryDetailsDto> SendCategoryDetailsRequest(int categoryId)
		{
			try
			{
				consoleLogger.Info("Availability request -- Api.Undercutters -- Data: ");

				var result = await apiClient.Get<CategoryDto>(null, baseConfig.CategoriesEndpoitn);
				consoleLogger.Info($"Availability response -- Api{api} -- Data: ", result);

				return new CategoryDetailsDto(result);
			}
			catch (Exception ex)
			{
				consoleLogger.Error("UndercuttersClient.GetProducts -- Exception", ex);
			}

			return new CategoryDetailsDto();
		}

		protected async Task<OrderDetailsDto> SendOrderDetailsRequest(int orderId)
		{
			try
			{
				consoleLogger.Info("Availability request -- Api.Undercutters -- Data: ");

				var result = await apiClient.Get<OrderDetailsDto>(null, baseConfig.OrdersEndpoint);
				consoleLogger.Info($"Availability response -- Api{api} -- Data: ", result);

				return new OrderDetailsDto();
			}
			catch (Exception ex)
			{
				consoleLogger.Error("UndercuttersClient.GetProducts -- Exception", ex);
			}

			return new OrderDetailsDto();
		}

		protected Task<OrderConfirmationDto> ConfirmOrder(OrderToConfirmRequest request)
		{
			throw new System.NotImplementedException();
		}

		protected async Task<BrandCollectionDto> SendBrandsRequest()
		{
			try
			{
				consoleLogger.Info("Availability request -- Api.Undercutters -- Data: ");

				var result = await apiClient.Get<List<BrandDto>>(null, baseConfig.OrdersEndpoint);
				consoleLogger.Info("Availability response -- Api.Undercutters -- Data: ", result);

				return new BrandCollectionDto();
			}
			catch (Exception ex)
			{
				consoleLogger.Error("UndercuttersClient.GetProducts -- Exception", ex);
			}

			return new BrandCollectionDto();
		}

		protected Task<BrandDetailsDto> SendBrandDetailsRequest(int brandId)
		{
			throw new System.NotImplementedException();
		}
	}
}
