﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ThAmCo.External.Apis.Clients.Interface;
using System;
using ThAmCo.External.Apis.Clients;

namespace ThAmCo.External.Apis.Clients
{
	public class ExternalApiClient : IExternalApiClient
	{
		public async Task<TResponse> Get<TResponse>(Dictionary<string, string> apiRequest, string url)
		{
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = new TimeSpan(10000);

                    var targetUrl = new StringBuilder(url)
                        .BuildQueryString(apiRequest)
                        .NormalizeUrl();

                    var response = await client.GetAsync(targetUrl);
                    var res = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<TResponse>(res);
                }
            }
            catch (TimeoutException ex)
            {
                return EmptyResponse.GetIstance<TResponse>();
            }
			
		}

  

		public Task<TResponse> Post<TResponse, TPayload>(TPayload playload, string url)
		{
			throw new System.NotImplementedException();
		}
	}

	public static class ExternalApiExtensions
	{
		public static bool ContainsOnlyOne(this Dictionary<string, string> dictionary)
		{
			return dictionary.Count == 1;
		}

		public static StringBuilder BuildQueryString(this StringBuilder stringBuilder, Dictionary<string, string> apiRequest)
		{
			if (apiRequest == null)
			{
				stringBuilder.Append("/");
				return stringBuilder;
			} 
			else if (apiRequest.ContainsOnlyOne())
			{
				stringBuilder.Append(apiRequest.First().Key + apiRequest.First().Value);
				return stringBuilder;
			}

			var queryString = new List<string>();

			foreach (var queryParam in apiRequest)
			{
				queryString.Add(queryParam.Key + "=" + queryParam.Value);
			}

			stringBuilder.Append("?");
			stringBuilder.Append(string.Join("&", queryString));
			return stringBuilder;
		}

		public static string NormalizeUrl(this StringBuilder strinbBuilder)
		{
			return strinbBuilder.ToString();
		}
	}
}