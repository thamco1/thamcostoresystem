﻿using System.Collections.Generic;
using ThAmCo.External.Apis.Dtos;

namespace ThAmCo.External.Apis.Clients
{
	public static class UrlParser
	{
		public static Dictionary<string, string> ParseProductsAvailabilityRequest(ProductAvailabilityRequest request)
		{
			var apiRequest = new Dictionary<string, string>
			{
				{"categoryId", request.CategoryId.ToString()},
				{"categoryName", request.CategoryName},
				{"brandId", request.BrandId.ToString()},
				{"maxPrice", request.MaxPrice.ToString()},
				{"minPrice", request.MinPrice.ToString()}
			};

			return apiRequest;
		}

		public static Dictionary<string, string> ParseProductDetailsRequest(int productId)
		{
			var apiRequest = new Dictionary<string, string>
			{
				{"/", productId.ToString()},
			};

			return apiRequest;
		}
	}
}
