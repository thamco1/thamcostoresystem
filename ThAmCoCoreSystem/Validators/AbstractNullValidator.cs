﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThAmCoCoreSystem.Validators
{
	using System.Threading.Tasks;

	using FluentValidation;
	using FluentValidation.Results;

	public abstract class NullAbstractValidator<TModel> : AbstractValidator<TModel> where TModel : class
	{
		public Task<ValidationResult> ValidateAsync(ValidationContext<TModel> context)
		{
			return context.InstanceToValidate == null
				? Task.FromResult(GetFailedResult<TModel>())
				: base.ValidateAsync(context);
		}

		public override ValidationResult Validate(TModel instance)
		{
			return instance == null
				? GetFailedResult<TModel>()
				: base.Validate(instance);
		}

		private static ValidationResult GetFailedResult<T>()
		{
			var type = typeof(T).Name;
			return new ValidationResult(new[] { new ValidationFailure(type, "Looks like the JSON isn't correct") });
		}
	}
}