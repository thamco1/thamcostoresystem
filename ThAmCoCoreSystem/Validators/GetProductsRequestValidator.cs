﻿using FluentValidation;
using ThAmCo.Web.Request;

namespace ThAmCoCoreSystem.Validators
{
	public class GetProductsRequestValidator : NullAbstractValidator<GetProductsRequest>
	{
		public GetProductsRequestValidator()
		{
			RuleFor(x => x.CategoryId).NotNull().GreaterThanOrEqualTo(1);
		}
	}
}