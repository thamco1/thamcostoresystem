﻿using System.Collections.Generic;
using System.Linq;
using ThAmCo.Common.Dtos;
using ThAmCo.Web.Request;
using ThAmCo.Web.Response;

namespace ThAmCoCoreSystem.ViewModels
{
	public class ProductsSearchVIewModel
	{
		public ProductsSearchVIewModel(ProductCollectionDto response)
		{
			this.Products = response.Products.Select(product => product);
		}

		public IEnumerable<ProductDto> Products { get; set; } 

	}
}