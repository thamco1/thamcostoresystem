﻿using ThAmCo.Common.Dtos;

namespace ThAmCoCoreSystem.ViewModels
{
	public class CategoryDetailsViewModel
	{
		public CategoryDetailsViewModel(CategoryDto result)
		{
			this.Category = result;
		}

		public CategoryDetailsViewModel()
		{
			
		}

		public CategoryDto Category { get; set; }
	}


}