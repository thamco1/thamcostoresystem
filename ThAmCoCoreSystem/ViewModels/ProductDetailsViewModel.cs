﻿using ThAmCo.Common.Dtos;

namespace ThAmCoCoreSystem.ViewModels
{
	public class ProductDetailsViewModel
	{
		public ProductDetailsViewModel(ProductDto response)
		{
			this.Product = response;
		}
		public ProductDto Product { get; set; }
	}
}