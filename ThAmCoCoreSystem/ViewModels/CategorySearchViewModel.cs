﻿using System.Collections.Generic;
using ThAmCo.Common.Dtos;

namespace ThAmCoCoreSystem.ViewModels
{
	public class CategorySearchViewModel
	{
		public CategorySearchViewModel(CategoryCollectionDto result)
		{
			this.Categories = result.Categories;
		}

		public IEnumerable<CategoryDto> Categories { get; set; } 

	}
}