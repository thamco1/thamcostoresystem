﻿using System.Collections.Generic;
using System.Web.Http;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using ThAmco.External.Feed;
using ThAmco.External.Feed.Demand;
using ThAmco.External.Feed.Demand.Interface;
using ThAmco.External.Feed.Provider;
using ThAmco.External.Feed.Provider.Interface;
using ThAmCo.Common.Dtos;
using ThAmCo.Common.Logs;
using ThAmCo.External.Apis.Clients;
using ThAmCo.External.Apis.Clients.Interface;
using ThAmCo.External.Apis.Config;
using ThAmCo.External.Apis.Config.Interface;
using ThAmCo.Services;

namespace ThAmCoCoreSystem.IoC
{
	public static class DependencyConfig
	{

		public static void SetupIoC(Container container)
		{
			ConfigProvider configProvider = new ConfigProvider
			{
				Undercutters = new UndercuttersConfig
				{
					CategoriesEndpoitn = "",
					OrdersEndpoint = "",
					ProductsEndpoint = "http://undercutters.azurewebsites.net/api/product",
					BrandsEndpoint = ""
				},
				Dodgydealers = new DodgydealersConfig
				{
					CategoriesEndpoitn = "",
					OrdersEndpoint = "",
					ProductsEndpoint = "http://dodgydealers.azurewebsites.net/api/product",
					BrandsEndpoint = "",
				},
				Khanskwikimart = new KhanskwikimartConfig
				{
					OrdersEndpoint = "",
					ProductsEndpoint = "",
					BrandsEndpoint = "",
					CategoriesEndpoitn = ""
				}
			};
			RegisterFeeds(container);
			RegisterDemands(container);
			RegisterConfigs(container, configProvider);
			RegisterLoggers(container);
			RegisterMappings();
			RegisterClients(container, configProvider);
			
			RegisterServices(container);

			container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

			GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
		}

		public static void RegisterDemands(Container container)
		{
			container.Register<IDemandProvider, DemandProvider>();
			container.Register<IDemandAvailabilityForProducts, DemandProviderProductsAvailability>();
		}
		public static void RegisterFeeds(Container container)
		{
			container.Register(() => GetApiFeeds(container));
		}

		public static void RegisterServices(Container container)
		{
			container.Register<IProductService, ProductService>();
		}

		public static void RegisterConfigs(Container container, ConfigProvider configProvider)
		{
			container.Register<IUndercuttersConfig>(() => new UndercuttersConfig
			{
				ProductsEndpoint = configProvider.Undercutters.ProductsEndpoint,
				BrandsEndpoint = configProvider.Undercutters.BrandsEndpoint,
				OrdersEndpoint = configProvider.Undercutters.OrdersEndpoint,
				CategoriesEndpoitn = configProvider.Undercutters.CategoriesEndpoitn
			});

			container.Register<IDodgydealersConfig>(() => new DodgydealersConfig
			{
				ProductsEndpoint = configProvider.Dodgydealers.ProductsEndpoint,
				BrandsEndpoint = configProvider.Dodgydealers.BrandsEndpoint,
				OrdersEndpoint = configProvider.Dodgydealers.OrdersEndpoint,
				CategoriesEndpoitn = configProvider.Dodgydealers.CategoriesEndpoitn
			});
			container.Register<IKhanskwikimartConfig, KhanskwikimartConfig>();
		}

		public static void RegisterLoggers(Container container)
		{
			container.Register<ILogger, ConsoleLogger>();
		}

		public static void RegisterMappings()
		{
			
		}

		private static IEnumerable<IProviderFeed> GetApiFeeds(Container container)
		{
			// Register your third party supplier feeds here
			var supplierFeeds = new List<IProviderFeed>
			{
				container.GetInstance<UndercuttersFeed>(),
				container.GetInstance<DodgydealersFeed>(),
				container.GetInstance<BazzasbazaarFeed>()
			};

			return supplierFeeds;
		}

		public static void RegisterClients(Container container, ConfigProvider configProvider)
		{
			
			// Register your third party clients here
			container.Register<IExternalApiClient, ExternalApiClient>();
			container.Register<IBazzasbazarClient, BazzasbazarClient>();
			container.Register<IUndercuttersClient, UndercuttersClient>();
			container.Register<IKhanskwikimartClient, KhanskwikimartClient>();
			container.Register<IDodgydealersClient, DodgydealersClient>();
		}
	}
}