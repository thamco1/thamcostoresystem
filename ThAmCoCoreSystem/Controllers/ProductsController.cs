﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using ThAmCo.Services;
using ThAmCo.Web.Request;
using ThAmCoCoreSystem.Validators;
using ThAmCoCoreSystem.ViewModels;

namespace ThAmCoCoreSystem.Controllers
{
	[RoutePrefix("api/products")]
    public class ProductsController : ApiController
	{
		private IProductService productService;

		public ProductsController(IProductService productService)
		{
			this.productService = productService;
		}

		[HttpGet]
		[Route("search")]
		public async Task<IHttpActionResult> SearchProducts([FromUri] GetProductsRequest request)
		{
			var validationResult = await new GetProductsRequestValidator().ValidateAsync(request);

			if (!validationResult.IsValid)
			{
				return BadRequest("Whoops! Looks like the request is invalid!");
			}

			var result = await this.productService.GetProductsAvailability(request);


			return Ok(result);
		}
    }
}
