﻿using System.Threading.Tasks;
using System.Web.Http;
using ThAmCo.External.Apis.Clients.Interface;
using ThAmCo.External.Apis.Dtos;

namespace ThAmCoCoreSystem.Controllers
{

	[RoutePrefix("api/undercutters")]
    public class UndercuttersController : ApiController
	{
		private IUndercuttersClient apiClient;

		public UndercuttersController(IUndercuttersClient client)
		{
			this.apiClient = client;
		}
		[HttpGet]
		[Route("products")]
		public async Task<IHttpActionResult> GetProducts()
		{
			var products = await apiClient.GetProductAvailability(new ProductAvailabilityRequest
			{
				MaxPrice = 0.99,
				BrandId = 1,
				CategoryId = 3,
				MinPrice = 10.99,
				CategoryName = "Example"
			});

			return Ok(products);
		}

		[HttpGet]
		[Route("products/details/{productId:int}")]
		public async Task<IHttpActionResult> GetProductDetails(int productId)
		{
			var product = await apiClient.GetProductDetails(productId);
			return Ok(product);
		}
    }
}
