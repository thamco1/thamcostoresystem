﻿using System.Web.Http;
using AutoMapper;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SimpleInjector;
using ThAmCoCoreSystem.IoC;

namespace ThAmCoCoreSystem
{
    public static class WebApiConfig
    {
		private static MapperConfiguration MapperConfiguration { get; set; }
	
        public static void Register(HttpConfiguration config)
        {
			Container container = new Container();

			DependencyConfig.SetupIoC(container);

            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


			// Json formatting settings
			var jsonSettings = config.Formatters.JsonFormatter.SerializerSettings;
			SetupJson(jsonSettings);
			config.Formatters.Remove(config.Formatters.XmlFormatter);
			SetupJson(jsonSettings);
			JsonConvert.DefaultSettings = () => jsonSettings;
		}

		public static void SetupJson(JsonSerializerSettings jsonSettings)
		{
			jsonSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
			jsonSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
			jsonSettings.TypeNameHandling = TypeNameHandling.None;
			jsonSettings.Formatting = Formatting.Indented;
		}
	}
}
