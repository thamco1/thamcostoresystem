﻿using System;

namespace ThAmCo.Common.Dtos
{
	public class ProductDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string Ean { get; set; }
		public bool IsInStock { get; set; }
		public double PriceForOne { get; set; }
		public double PriceForTen { get; set; }
		public DateTime? ExpectedRestock { get; set; }
	}
}
