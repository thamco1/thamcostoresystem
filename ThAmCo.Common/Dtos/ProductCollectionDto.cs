﻿using System.Collections;
using System.Collections.Generic;

namespace ThAmCo.Common.Dtos
{
	public class ProductCollectionDto
	{
		public IEnumerable<ProductDto> Products { get; set; }
	}
}
