﻿using System.Collections;
using System.Collections.Generic;

namespace ThAmCo.Common
{
	public class BrandCollectionDto
	{
		public IEnumerable<BrandDto> Brands { get; set; }
	}
}
