﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThAmCo.Common.Dtos;

namespace ThAmCo.Common
{
	public class ProductAvailabilityDto
	{
		public IEnumerable<ProductDto> Products { get; set; }

		public ProductAvailabilityDto()
		{
			this.Products = new List<ProductDto>();
		} 

	}
}
