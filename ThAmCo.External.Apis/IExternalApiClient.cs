﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ThAmCo.External.Apis
{
	public interface IExternalApiClient
	{
		Task<TResponse> Get<TResponse>(Dictionary<string, string> apiRequest, string url);
		Task<TResponse> Post<TResponse, TPayload>(TPayload playload, string url);
	}
}
