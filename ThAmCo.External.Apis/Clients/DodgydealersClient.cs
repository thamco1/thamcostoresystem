﻿using ThAmCo.External.Apis.Configuration;

namespace ThAmCo.External.Apis.Clients
{
	public class DodgydealersClient : IDodgydealersClient
	{

		private readonly IDodgydealersConfig apiConfig;
		private readonly IExternalApiClient apiClient;

		public DodgydealersClient(IDodgydealersConfig apiConfig, IExternalApiClient apiClient)
		{
			this.apiConfig = apiConfig;
			this.apiClient = apiClient;
		}

	}
}