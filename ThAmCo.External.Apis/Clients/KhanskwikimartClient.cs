﻿using ThAmCo.External.Apis.Configuration;

namespace ThAmCo.External.Apis.Clients
{
	public class KhanskwikimartClient : IKhanskwikimartClient
	{
		private readonly IKhanskwikimartConfig apiConfig;
		private readonly IExternalApiClient apiClient;

		public KhanskwikimartClient(IKhanskwikimartConfig apiConfig, IExternalApiClient apiClient)
		{
			this.apiConfig = apiConfig;
			this.apiClient = apiClient;
		}
	}
}