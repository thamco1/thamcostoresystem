﻿using ThAmCo.External.Apis.Configuration;

namespace ThAmCo.External.Apis.Clients
{
	public class UndercuttersClient : IUndercuttersClient
	{
		private readonly IUndercuttersConfig apiConfig;
		private readonly IExternalApiClient apiClient;

		public UndercuttersClient(IUndercuttersConfig apiConfig, IExternalApiClient apiClient)
		{
			this.apiConfig = apiConfig;
			this.apiClient = apiClient;
		}
	}
}