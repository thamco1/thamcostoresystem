﻿using System.Threading.Tasks;
using ThAmco.External.Feed.Provider.Interface;
using ThAmCo.External.Apis.Clients.Interface;
using ThAmCo.External.Apis.Dtos;

namespace ThAmco.External.Feed.Provider
{
	public class BazzasbazaarFeed : IProviderFeed
	{
		private IBazzasbazarClient apiClient;

		public BazzasbazaarFeed(IBazzasbazarClient apiClient)
		{
			this.apiClient = apiClient;
		}

		public Task<ProductAvailabilityDto> GetProductAvailability(ProductAvailabilityRequest request)
		{
			return this.apiClient.GetProductAvailability(request);
		}

		public Task<ProductDetailsDto> GetProductDetails(int productId)
		{
			throw new System.NotImplementedException();
		}
	}
}
