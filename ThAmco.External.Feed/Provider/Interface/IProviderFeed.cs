﻿using System.Threading.Tasks;
using ThAmCo.External.Apis.Dtos;

namespace ThAmco.External.Feed.Provider.Interface
{
	public interface IProviderFeed
	{
		Task<ProductAvailabilityDto> GetProductAvailability(ProductAvailabilityRequest request);
		Task<ProductDetailsDto> GetProductDetails(int productId);
	}
}
