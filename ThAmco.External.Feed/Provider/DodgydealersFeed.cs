﻿using System.Threading.Tasks;
using ThAmco.External.Feed.Provider.Interface;
using ThAmCo.External.Apis.Clients.Interface;
using ThAmCo.External.Apis.Dtos;

namespace ThAmco.External.Feed.Provider
{
	public class DodgydealersFeed : IProviderFeed
	{
		private readonly IDodgydealersClient apiClient;

		public DodgydealersFeed(IDodgydealersClient apiClient)
		{
			this.apiClient = apiClient;
		}

		public async Task<ProductAvailabilityDto> GetProductAvailability(ProductAvailabilityRequest request)
		{
			return await this.apiClient.GetProductAvailability(request);
		}

		public Task<ProductDetailsDto> GetProductDetails(int productId)
		{
			throw new System.NotImplementedException();
		}
	}
}
