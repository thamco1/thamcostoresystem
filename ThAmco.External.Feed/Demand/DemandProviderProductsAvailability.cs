using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ThAmco.External.Feed.Demand.Interface;
using ThAmco.External.Feed.Provider.Interface;
using ThAmCo.External.Apis.Dtos;

namespace ThAmco.External.Feed
{
	public class DemandProviderProductsAvailability : IDemandAvailabilityForProducts
	{
		private readonly IEnumerable<IProviderFeed> apiFeeds;

		public Api? Api { get; set; }
		public int CategoryId { get; set; }
		public string CategoryName { get; set; }
		public int BrandId { get; set; }
		public double? MinPrice { get; set; }
		public double? MaxPrice { get; set; }

		public DemandProviderProductsAvailability(IEnumerable<IProviderFeed> apiFeeds)
		{
			this.apiFeeds = apiFeeds;
		}

		public async Task<IEnumerable<ProductAvailabilitiesDto>> ExecuteDemandAsync()
		{
			var tasks = apiFeeds.Select(async feed =>
			{
				var request = new ProductAvailabilityRequest
				{
					CategoryId = this.CategoryId,
					CategoryName = this.CategoryName,
					BrandId = this.BrandId,
					MinPrice = this.MinPrice,
					MaxPrice = this.MaxPrice
				};

				var availability = await feed.GetProductAvailability(request);
				return availability.Products;
			});

			var awaitTasks = await Task.WhenAll(tasks);

			var selectMany = awaitTasks.Where(task => task != null).SelectMany(task => task);
			var avail = selectMany.Where(x => x.InStock).ToList();

			var products = avail.Select(x => new ProductAvailabilitiesDto
			{
				BrandId = x.BrandId,
				Description = x.Description,
				Ean = x.Ean,
				Name = x.Name,
				PriceForTen = (double)x.Price * 10,
				ExpectedRestock = x.ExpectedRestock,
				IsInStock = x.InStock,
				Id = x.Id,
				PriceForOne = (double)x.Price
			});
			return products;
		}
	}
}