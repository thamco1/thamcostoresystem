﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ThAmco.External.Feed.Demand.Interface;
using ThAmco.External.Feed.Provider.Interface;
using ThAmCo.External.Apis.Dtos;

namespace ThAmco.External.Feed.Demand
{
	public class DemandProviderProductDetails : IDemandProviderProductDetails
	{
		private readonly IEnumerable<IProviderFeed> apiFeeds;

		public DemandProviderProductDetails(IEnumerable<IProviderFeed> apiFeeds)
		{
			this.apiFeeds = apiFeeds;
		} 

		public async Task<IEnumerable<ProductAvailabilitiesDto>> ExecuteDemandAsync()
		{
			var tasks = apiFeeds.Select(async feed =>
			{
				var productDetails = await feed.GetProductDetails(this.ProductId);
				return productDetails;
			});

			var awaitTasks = await Task.WhenAll(tasks);

			var completedTasks = awaitTasks.Where(task => task != null);

			var productDeails = completedTasks.Select(x => new ProductAvailabilitiesDto
			{
				Description = x.Product.Description,
				BrandId = x.Product.BrandId,
				Id = x.Product.Id,
				Name = x.Product.Name,
				Ean = x.Product.Ean,
				ExpectedRestock = x.Product.ExpectedRestock,
				PriceForOne = (double)x.Product.Price,
				IsInStock = x.Product.InStock
			});
			return productDeails;
		}

		public int ProductId { get; set; }
	}
}
