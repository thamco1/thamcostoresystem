﻿using SimpleInjector;
using ThAmco.External.Feed.Demand.Interface;

namespace ThAmco.External.Feed.Demand
{
	public class DemandProvider : IDemandProvider
	{
		private readonly Container container;

		public DemandProvider(Container container)
		{
			this.container = container;
		}

		public TModel Create<TModel>() where TModel : class, IDemand
		{
			return this.container.GetInstance<TModel>();
		}
	}
}
