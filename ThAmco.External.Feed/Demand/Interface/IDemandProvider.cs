﻿namespace ThAmco.External.Feed.Demand.Interface
{
	public interface IDemandProvider
	{
		TModel Create<TModel>() where TModel : class, IDemand;
	}
}
