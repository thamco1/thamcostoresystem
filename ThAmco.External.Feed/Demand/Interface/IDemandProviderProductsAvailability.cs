﻿using System.Collections.Generic;
using ThAmCo.External.Apis.Dtos;

namespace ThAmco.External.Feed.Demand.Interface
{
	public interface IDemandAvailabilityForProducts : IDemand<IEnumerable<ProductAvailabilitiesDto>>
	{

		Api? Api { get; set; }
		int CategoryId { get; set; }
		string CategoryName { get; set; }
		int BrandId { get; set; }
		double? MinPrice { get; set; }
		double? MaxPrice { get; set; }
	}
}
