﻿using System.Collections.Generic;
using ThAmCo.External.Apis.Dtos;

namespace ThAmco.External.Feed.Demand.Interface
{
	public interface IDemandProviderProductDetails : IDemand<IEnumerable<ProductAvailabilitiesDto>>
	{
		int ProductId { get; set; }
	}
}
