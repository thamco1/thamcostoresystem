﻿using System.Threading.Tasks;

namespace ThAmco.External.Feed.Demand.Interface
{
	public interface IDemand<TQueryResult> : IDemand
	{
		Task<TQueryResult> ExecuteDemandAsync();
	}

	public interface IDemand { }
}
