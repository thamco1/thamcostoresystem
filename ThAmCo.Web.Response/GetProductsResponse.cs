﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ThAmCo.Common.Dtos;

namespace ThAmCo.Web.Response
{
    public class GetProductsResponse
    {

	    public GetProductsResponse()
	    {
		    this.Products = Enumerable.Empty<ProductDto>();
	    }

		public IEnumerable<ProductDto> Products { get; set; }
    }
}
