﻿namespace ThAmCo.WebJobs.ContentUpdater
{
    using Microsoft.Azure.WebJobs;
    class Program
    {

        static void Main()
        {
            var host = new JobHost();
            host.RunAndBlock();
        }

    }
}
