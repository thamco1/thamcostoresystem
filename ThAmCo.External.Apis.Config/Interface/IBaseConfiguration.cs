﻿namespace ThAmCo.External.Apis.Config.Interface
{
	public interface IBaseConfiguration
	{
		string ProductsEndpoint { get; set; }
		string CategoriesEndpoitn { get; set; }
		string OrdersEndpoint { get; set; }
		string BrandsEndpoint { get; set; }
	}
}
