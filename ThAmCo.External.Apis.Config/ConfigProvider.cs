﻿namespace ThAmCo.External.Apis.Config
{
	public class ConfigProvider
	{
		public UndercuttersConfig Undercutters { get; set; }
		public DodgydealersConfig Dodgydealers { get; set; }
		public KhanskwikimartConfig Khanskwikimart { get; set; }
	}
}
