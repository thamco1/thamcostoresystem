﻿using ThAmCo.External.Apis.Config.Interface;

namespace ThAmCo.External.Apis.Config
{
	public class KhanskwikimartConfig : IKhanskwikimartConfig
	{
		public string ProductsEndpoint { get; set; }
		public string CategoriesEndpoitn { get; set; }
		public string OrdersEndpoint { get; set; }
		public string BrandsEndpoint { get; set; }
	}
}