﻿namespace ThAmCo.Web.Request
{
    public class GetProductsRequest
    {
		public int CategoryId { get; set; }
		public string CategoryName { get; set; }
		public double? MinPrice { get; set; }
		public double? MaxPrice { get; set; }
		public int BrandId { get; set; }
		public string Api { get; set; }
	}
}
