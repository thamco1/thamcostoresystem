﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThAmCo.Web.Request
{
	public class ProviderResponse<TData>
	{
		public ResponseType ResponseType { get; set; }
		public IEnumerable<TData> Data { get; set; } 
	}
}
